build: lint
	docker build -t yamllint .
	docker run --rm -v "$(shell pwd):/work" yamllint yamllint . | tee yamllint.log

push_major:
	git status
	bump2version major
	git push
	git push --tags

push_minor:
	git status
	bump2version minor
	git push
	git push --tags

push_patch:
	git status
	bump2version patch
	git push
	git push --tags

.PHONY: build lint yamllint textlint gitlab-ci-yaml_lint push_patch push_minor push_major

lint: yamllint textlint gitlab-ci-yaml_lint

yamllint:
	docker run --rm registry.gitlab.com/jeff_cook/yamllint yamllint --version
	docker run --rm -v "$(shell pwd):/work" registry.gitlab.com/jeff_cook/yamllint yamllint . | tee yamllint.log

textlint:
	docker pull registry.gitlab.com/jeff_cook/textlint
	docker run --rm registry.gitlab.com/jeff_cook/textlint textlint --version
	# docker run --rm -v $(shell pwd):/work registry.gitlab.com/jeff_cook/textlint textlint --config .textlintrc.yaml --format pretty-error .
	docker run --rm -v $(shell pwd):/code/ --workdir /code registry.gitlab.com/jeff_cook/textlint | tee textlint.log

gitlab-ci-yaml_lint:
	echo 'FROM node:current-alpine\nRUN npm install -g gitlab-ci-lint' | docker build -t gitlab-ci-lint -
	docker run --rm -it -v $(shell pwd)/:/code/ --workdir /code/ gitlab-ci-lint gitlab-ci-lint --url https://gitlab.com .gitlab-ci/.gitlab-ci.yml
	# docker run --rm -it -v $(shell pwd)/:/code/ --workdir /code/ gitlab-ci-lint gitlab-ci-lint --url https://gitlab.com .gitlab-ci.yml
