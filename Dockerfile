FROM python:3.9.0-alpine3.12

COPY requirements.txt requirements.txt
RUN pip install --requirement requirements.txt

RUN mkdir -p /work
WORKDIR /work

RUN addgroup yamllint && adduser -D -G yamllint yamllint
USER yamllint

RUN mkdir -p ~/.config/yamllint
COPY .yamllint.yaml ~/.config/yamllint/config
