## [0.4.3](https://gitlab.com/jeff_cook/yamllint/compare/v0.4.2...v0.4.3) (2020-08-08)


### Bug Fixes

* simplify pipeline ([db125af](https://gitlab.com/jeff_cook/yamllint/commit/db125afcb3e03bccd397bb552f55894ca37cb48d))
* update python and alpine version ([263532b](https://gitlab.com/jeff_cook/yamllint/commit/263532b8d6ddd68e24c2cfc54ac8b08bf813d0cd))
